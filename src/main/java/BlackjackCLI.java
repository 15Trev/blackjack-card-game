import java.util.List;

import card.deck.Card;
import card.deck.CardDeck;
import money.InvalidAmountException;
import money.MoneySystem;
import view.Menu;

public class BlackjackCLI {
	
	private final static char[] MAIN_MENU_OPTIONS = {'1', '2', 'Q' };
	private final static String[] MAIN_MENU = { "Deposit Funds", "Start Game", "Quit/Withdraw Funds" };
	
	private final static char[] GAME_MENU_OPTIONS = {'0', '1', '2', '3', 'Q' };
	private final static String[] GAME_MENU = { "Hit", "Stay", "Split", "Surrender", "Quit" };

	private Menu menu;
	private BlackjackGame blackjackGame;
	
	public BlackjackCLI(Menu menu) {
		this.menu = menu;
	}
	
	public void run() {	
		this.blackjackGame = getBlackjackGame();
		
		boolean running = true;
		while (running) {
			String userBalanceDisplay = String.format("Account Balance: $%-9.2f", blackjackGame.getUserBalance());
			int choice = menu.getChoiceFromMenu(MAIN_MENU, MAIN_MENU_OPTIONS, userBalanceDisplay);
			switch (choice) {
				case 1:
					addFunds();
					break;
				case 2:
					if (getWagerAmount()) {
						startGame();
					}
					break;
				case 3:
					menu.exitMessage(blackjackGame.getUserBalance());
					running = false;
					break;
			}			
			
		}
	}

	private void addFunds() {
		while (true) {
			String amountInput = menu.getUserInput("Amount to add >>> $");
			
			try {
				double amount = Double.parseDouble(amountInput);
				
				if (!blackjackGame.addFunds(amount)) {
					menu.showMessage("Cannot add negative amount.");
					continue;
				}
			} catch (NumberFormatException e) {
				menu.showMessage("Invalid amount.");
				continue;
			} catch (InvalidAmountException e) {
				menu.showMessage(e.getMessage());
				continue;
			}
			break;
		}
	}
	
	private boolean getWagerAmount() {
		while (true) {
			String amountInput = menu.getUserInput("Bet amount (0 to return) >>> $");
			
			try {
				double amount = Double.parseDouble(amountInput);
				
				if (amount == 0) {
					return false;
				}
				
				else if (!blackjackGame.wagerFunds(amount)) {
					menu.showMessage("Cannot wager negative amount.");
					continue;
				}
				
			} catch (NumberFormatException e) {
				menu.showMessage("Invalid Amount.");
				continue;
			} catch (InvalidAmountException e) {
				menu.showMessage(e.getMessage());
				continue;
			}
			return true;
		}
	}
	
	private void startGame() {
		blackjackGame.startNewGame();
		
		boolean gameRunning = true;
		while (gameRunning) {
			
			List<Card> dealerHand = blackjackGame.getDealerHand();
			List<Card> playerHand = blackjackGame.getPlayerHand();
			int dealerPoints = blackjackGame.getPoints(dealerHand);
			int playerPoints = blackjackGame.getPoints(playerHand);
			
			menu.displayCards(dealerHand, playerHand, playerPoints);
			
			int choice = menu.getChoiceFromMenu(GAME_MENU, GAME_MENU_OPTIONS);
			switch (choice) {
				case 1:
//					addFunds();
//					break;
				case 2:
//					if (getWagerAmount()) {
//						startGame();
//					}
//					break;
				case 5:
					gameRunning = false;
					break;
			}
		}
	}

	private BlackjackGame getBlackjackGame() {
		return new BlackjackGame(new MoneySystem(), new CardDeck());
	}
	
	public static void main(String[] args) {
		Menu menu = new Menu();
		BlackjackCLI cli = new BlackjackCLI(menu);
		cli.run();
	}

}
