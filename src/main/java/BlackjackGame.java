

import java.util.ArrayList;
import java.util.List;

import card.deck.Card;
import card.deck.CardDeck;
import money.InvalidAmountException;
import money.MoneySystem;

public class BlackjackGame {
	
	private final static double MAXIMUM_ALLOWED_BALANCE = 9999999.99;
	
	private MoneySystem moneySystem;
	private CardDeck cardDeck;
	private Card card;
	private List<Card> dealerHand = new ArrayList<Card>();
	private List<Card> playerHand = new ArrayList<Card>();
	
	public BlackjackGame(MoneySystem moneySystem, CardDeck cardDeck) {
		this.moneySystem = moneySystem;
		this.cardDeck = cardDeck;
	}
	
	
	public double getUserBalance() {
		return moneySystem.getBalance();
	}
	
	public double getWagerAmount() {
		return moneySystem.getWager();
	}

	public boolean addFunds(double amount) throws InvalidAmountException {
		
		double newBalance = moneySystem.getBalance() + amount;
		
		if (newBalance > MAXIMUM_ALLOWED_BALANCE) {
			throw new InvalidAmountException("Amount would exceed the maximum allowed balance of $" + MAXIMUM_ALLOWED_BALANCE, newBalance);
		}
		
		boolean wasSuccessful = moneySystem.addFunds(amount);
	
		return wasSuccessful;
	}
	
	public boolean wagerFunds(double amount) throws InvalidAmountException {
		
		double newBalance = moneySystem.getBalance() - amount;
		
		if (newBalance < 0) {
			throw new InvalidAmountException("Bet amount would exceed current balance.", newBalance);
		}
		
		boolean wasSuccessful = moneySystem.setWager(amount);
	
		return wasSuccessful;
	}
	
	public void startNewGame() {
		cardDeck.createDeck();
		cardDeck.shuffle();
		dealStartingCards();
	}
	
	public void dealStartingCards() {
		dealerHand.add(cardDeck.dealCard());
		dealerHand.add(cardDeck.dealCard());
		playerHand.add(cardDeck.dealCard());
		playerHand.add(cardDeck.dealCard());
	}
	
	public int getPoints(List<Card> cards) {
		int sum = 0;
		for(Card c: cards) {
			sum += getCardValue(c);
		}
		return sum;
	}
	
	private int getCardValue(Card card) {
		switch(card.getRank()) {
			case ACE:
				return 1;
			case TWO:
				return 2;
			case THREE:
				return 3;
			case FOUR:
				return 4;
			case FIVE:
				return 5;
			case SIX:
				return 6;
			case SEVEN:
				return 7;
			case EIGHT:
				return 8;
			case NINE:
				return 9;
			case TEN:
				return 10;
			case JACK:
				return 10;
			case QUEEN:
				return 10;
			case KING:
				return 10;
			default:
				return 0;
		}
	}
	
	
	public MoneySystem getMoneySystem() {
		return moneySystem;
	}

	public CardDeck getCardDeck() {
		return cardDeck;
	}

	public List<Card> getDealerHand() {
		return dealerHand;
	}

	public List<Card> getPlayerHand() {
		return playerHand;
	}
	
}
