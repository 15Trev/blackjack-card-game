package card.deck;

import card.deck.CardDeck.Ranks;
import card.deck.CardDeck.Suits;

public class Card {
	
	private Suits suit;
	private Ranks rank;
	
	public Card (Suits suit, Ranks rank){
		this.suit = suit;
		this.rank = rank;
	}
	
	
	public Suits getSuit() {
		return suit;
	}

	public Ranks getRank() {
		return rank;
	}

}
