package card.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardDeck {
	
	private List<Card> cards = new ArrayList<Card>();
	
	public enum Suits {
		SPADES,
		HEARTS,
		CLUBS,
		DIAMONDS
	}
	
	public enum Ranks {
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		TEN,
		JACK,
		QUEEN,
		KING,
		ACE
	}
	
	public List<Card> createDeck() {
		this.cards = new ArrayList<Card>();
		for (Suits s : Suits.values()) {
			for (Ranks r : Ranks.values()) {
				Card card = new Card(s, r);
				this.cards.add(card);
			}
		}
		return this.cards;
	}
	
	public void shuffle() {
		Collections.shuffle(this.cards);
	}
	
	public Card dealCard() {
		if (cards.isEmpty()) {
			return null;
		}
		
		Card selectedCard = cards.remove(0);
		return selectedCard;
	}
}
