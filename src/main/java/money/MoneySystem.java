package money;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MoneySystem {

	private BigDecimal balance;
	private BigDecimal wager;

	public MoneySystem() {
		resetBalance();
		resetWager();
	}
	
	public boolean enoughBalanceForBet(double betAmount) {
		return balance.subtract(new BigDecimal(betAmount)).compareTo(BigDecimal.ZERO) >= 0;
	}
	
	public boolean addFunds(double amount) {
		if (amount < 0) {
			return false;
		}
		balance = balance.add(new BigDecimal(amount));
		return true;
	}
	
	public double cashOut() {
		double currentBalance = getBalance();
		resetBalance();
		return currentBalance;	
	}
	
	private void resetBalance() {
		balance = BigDecimal.ZERO;
	}
	
	private void resetWager() {
		wager = BigDecimal.ZERO;
	}
	
	public double getBalance() {
		return balance.setScale(2, RoundingMode.HALF_UP).doubleValue();
	}
	
	public double getWager() {
		return wager.setScale(2, RoundingMode.HALF_UP).doubleValue();
	}

	public boolean setWager(double wager) {
		if (wager >= 0 && enoughBalanceForBet(wager)) {
			this.wager = new BigDecimal(wager);
			this.balance = balance.subtract(new BigDecimal(wager));
			return true;
		}
		return false;
	}
}
