package view;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import card.deck.Card;

public class Menu {
	private Scanner in = new Scanner(System.in);
	
	public String getUserInput(String question) {
		System.out.print(question);
		return in.nextLine();
	}
	
	public void showErrorMessage(String message) {
		displayMessage(message, true);
		System.out.println();
	}
	
	public void showMessage(String message) {
		displayMessage(message, false);
		System.out.println();
	}
	
	public void displayCards(List<Card> dealerCards, List<Card> playerCards, int playerPoints) {
		System.out.println("\nDealer has: ");
		System.out.print(cardPrint(dealerCards));
		System.out.println("-----------------\nYou have: ");
		System.out.println(cardPrint(playerCards));
		System.out.print(playerPoints);
	}
	
	private String cardPrint(List<Card> cards) {
		String hand = "";
		for(Card c : cards) {
			hand += c.getRank() + " of " + c.getSuit() + "\n";
		}
		return hand;
	}
	
	private void displayMessage(String message, boolean isError) {
		System.out.println( (isError ? "ERROR: " : "") + message);
		System.out.println();
	}
	
	public int getChoiceFromMenu(String[] menuItems, char[] menuOptions) {
		return getChoiceFromMenu(menuItems, menuOptions, null);
	}
	
	public int getChoiceFromMenu(String[] menuItems, char[] menuOptions, String infoMessage) {
		
		System.out.println();
		
		int userChoice = 0;
		while (true) {
			for (int i = 0; i < menuItems.length; i++) {
				System.out.printf("(%1s) %-20s%n", menuOptions[i], menuItems[i] );
			}
			System.out.println();
			if (infoMessage != null) {
				System.out.println(infoMessage);
				System.out.println();
			}
			System.out.print("Select >>> ");
			String choice = in.nextLine();
			
			if (!choice.substring(0, 1).equalsIgnoreCase("Q")) {
			
				try {
					userChoice = Integer.parseInt(choice);
				} catch (NumberFormatException e) {
					showInvalidChoiceError();
					continue;
				}
				
				
				if (userChoice < 0 || userChoice >= menuItems.length) {
					showInvalidChoiceError();
					continue;
				}
				
			} else {
				userChoice = menuItems.length;
			}
			
			break;	
		}
		System.out.println();
		return userChoice;
	}
	
	private void showInvalidChoiceError() {
		System.out.println("Invalid choice.  Please make another selection.");
		System.out.println();
	}

	public void exitMessage(double balance) {
		System.out.printf("Your returned change is: $%-9.2f\n", balance);
		System.out.println("Thank you for playing!");
		
	}
}
